<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@showIndexPage');
Route::get('/contact-us', 'HomeController@showContactPage');
Route::get('/about-us', 'HomeController@showAboutPage');
Route::get('/blogs', 'BlogController@index');
Route::get('/blogs/{slug}', 'BlogController@show');




Route::get('admin/login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);

Route::post('admin/login', [
    // 'as' => 'login',
    'uses' => 'Auth\LoginController@login'
]);
Route::get('admin/register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('admin/register', [
    // 'as' => 'register',
    'uses' => 'Auth\RegisterController@register'
]);
Route::post('admin/logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

Route::prefix('admin')->namespace('Admin')->middleware('auth')->group(static function () {
    Route::get('', 'HomeController@showAdminDashboard');
    Route::resource('teams', 'TeamController');
    Route::resource('blogs', 'BlogController');
    Route::resource('services', 'ServiceController');
    Route::resource('projects', 'ProjectController');
    Route::resource('clients', 'ClientController');
});
