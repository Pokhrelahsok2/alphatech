@extends('user.layouts.app')
@section('content')
    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__option">
                        <a href="/"><span class="fa fa-home"></span> Home</a>
                        <span>Not Found</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- 404 Section Begin -->
    <section class="section-404 spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text__404">
                    <img src="{{asset('storage/img/404.png')}}" alt="">
                        <h3>Opps! This page Could Not Be Found!</h3>
                        <p>Sorry bit the page you are looking for does not exist, have been removed or name changed</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- 404 Section End -->
@endsection