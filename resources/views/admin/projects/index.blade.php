@extends('admin.layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox collapsed">
            <div class="ibox-title collapse-link">
                <h5>Add a New Project</h5>
                <div class="ibox-tools">
                    <a>
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <form action="{{ action('Admin\ProjectController@store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="type">Type</label>
                                <input type="text" name="type" id="type" placeholder="" class="form-control
                                                                @error('type') is-invalid @enderror">

                                @error('type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" placeholder="" class="form-control
                                    @error('title') is-invalid @enderror">

                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="links">Links</label>
                                <input type="text" name="links" id="links" placeholder="" class="form-control
                                    @error('links') is-invalid @enderror">

                                @error('skills')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" name="description" id="description" placeholder=""
                                    class="form-control" @error('description') is-invalid @enderror">

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 text-left">
                            <div class="form-group">
                                <button class="btn btn-primary">Add Project</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>All Projects</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <table class="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Type</th>
                            <th>Title</th>
                            <th>Links</th>
                            <th>Description</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($projects as $key=>$project)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $project->type }}</td>
                            <td>{{ $project->title }}</td>
                            <td>{{ $project->links }}</td>
                            <td>{{ $project->description }}</td>
                            <th>
                                <form action="{{ action('Admin\ProjectController@destroy', $project->id) }}"
                                    method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <a title="Edit This Project"
                                        href="{{ action('Admin\ProjectController@edit', $project->id) }}"
                                        class="btn btn-info">
                                        <span class="fa fa-edit"></span> Edit
                                    </a>
                                    <button class="btn btn-danger"><span class="fa fa-trash"></span> Delete</button>
                                </form>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection