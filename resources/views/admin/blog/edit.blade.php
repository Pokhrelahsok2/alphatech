@extends('admin.layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox collapsed">
            <div class="ibox-title collapse-link">
                <h5>Add a New Blog</h5>
                <div class="ibox-tools">
                    <a>
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <form action="{{ action('Admin\BlogController@store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" placeholder="" class="form-control @error('title') is-invalid @enderror" required>

                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="type">Type</label>
                                
                                <select id="type" class="form-control @error('type') is-invalid @enderror" name="account" required>
                                    <option value="Programming">Programming</option>
                                    <option value="Smartphones">Smartphones</option>
                                    <option value="Science">Science</option>
                                    <option value="Laptops">Laptops</option>
                                </select>
                                @error('type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label for="image">Thumbnail</label>
                                <input type="file" name="image" id="image" placeholder="" class="form-control
                                    @error('image') is-invalid @enderror">

                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="body">Body</label>
                                <div id="gjs">
                                
                                
                                </div>
                                <input type="hidden" name="body" id="body">
                                @error('body')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 text-left">
                            <div class="form-group">
                                <button class="btn btn-primary">Add Blog</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>All Blogs</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Title</th>
                            <th>Type</th>
                            <th>Author</th>
                            <th>Thumbnail</th>
                            <th>Body</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($blogs as $key=>$blog)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $blog->title }}</td>
                            <td>{{ $blog->type }}</td>
                            <td>{{ $blog->author->name }}</td>
                            <td><a href="{{Storage::url($blog->image)}}" target="_blank"> <img
                                src="{{Storage::url($blog->image)}}" width="80"></a></td>
                            <td>{{ $blog->body }}</td>
                            <th>
                                <form action="{{ action('Admin\BlogController@destroy', $blog->id) }}"
                                    method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <a title="Edit This Blog"
                                        href="{{ action('Admin\BlogController@edit', $blog->id) }}"
                                        class="btn btn-info">
                                        <span class="fa fa-edit"></span> Edit
                                    </a>
                                    <button class="btn btn-danger"><span class="fa fa-trash"></span> Delete</button>
                                </form>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <?php echo $blogs->render(); ?>
            </div>
        </div>
    </div>
</div>

@endsection
{{-- 
@section('scripts')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {
    $('#body').summernote();
    });
</script>
@endsection --}}


@section('scripts')
        <link rel="stylesheet" href="https://unpkg.com/grapesjs/dist/css/grapes.min.css">
        <script src="https://unpkg.com/grapesjs"></script>
        <script src="/js/grapesjs-preset-webpage.min.js"></script>
        <link rel="stylesheet" href="/css/grapesjs-preset-webpage.min.css">
        <script>
             var editor = grapesjs.init({
                container : '#gjs',
                plugins: ['gjs-preset-newsletter'],
                storageManager: {
                    type: null
                },
                pluginsOpts: {
                    'gjs-preset-newsletter': {
                    modalTitleImport: 'Import template',
                    }
                }
            });
            $('form').on('submit',function(e){
                let htmlWithCss = editor.runCommand('gjs-get-inlined-html');
                $('#body').val(htmlWithCss);
            })

        </script>
@endsection