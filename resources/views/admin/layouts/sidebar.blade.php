<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <a class="nav-link pr-0 text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <span class="block m-t-xs font-bold">{{ Auth::user()->name }}</span>
                </a>
            </li>
            <li {{ (Request::is('/') ? 'class=active' : '') }}>
                <a href="{{ action('Admin\HomeController@showAdminDashboard') }}"><i class="fa fa-home"></i> <span
                        class="nav-label">Dashboard</span></a>
            </li>
            <li {{ (Request::is('admin/teams') ? 'class=active' : '') }}>
                <a href="{{ action('Admin\TeamController@index') }}"><i class="fa fa-users"></i> <span
                        class="nav-label">Teams</span></a>
            </li>
            <li {{ (Request::is('admin/services') ? 'class=active' : '') }}>
                <a href="{{ action('Admin\ServiceController@index') }}"><i class="fa fa-clone "></i> <span
                        class="nav-label">Services</span></a>
            </li>
            <li {{ (Request::is('admin/projects') ? 'class=active' : '') }}>
                <a href="{{ action('Admin\ProjectController@index') }}"><i class="fa fa-database "></i> <span
                        class="nav-label">Projects</span></a>
            </li>
            <li {{ (Request::is('admin/clients') ? 'class=active' : '') }}>
                <a href="{{ action('Admin\ClientController@index') }}"><i class="fa fa-users "></i> <span
                        class="nav-label">Clients</span></a>
            </li>
            <li {{ (Request::is('admin/blogs') ? 'class=active' : '') }}>
                <a href="{{ action('Admin\BlogController@index') }}"><i class="fa fa-blog "></i> <span
                        class="nav-label">Blogs</span></a>
            </li>
            <li class="/nav-item">
                <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                    class="nav-label d-flex align-items-center">
                    <i class="fas fa-sign-out-alt"></i> &nbsp;&nbsp;
                    <span>Log Out</span>
                </a>
                @include('forms.logout')
            </li>
        </ul>
    </div>
</nav>