<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    ​
    <title>{{ config('app.name', 'Alphatech') }}</title>
    <link rel="icon" href="{{asset('storage/img/icon.png')}}" type="image/x-icon">
​     <link rel="stylesheet" href="{{mix('css/admin.css')}}">
    <style>
        #toast-container {
            margin-top: 10px !important;
        }
    </style>

    @yield("css")
</head>
​

<body>
    <div id="wrapper">
        @include('admin.layouts.sidebar')
        <div id="page-wrapper" class="gray-bg dashboard-1">
            <div class="row">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            </div>
            @yield('content')
        </div>
    </div>

    
    ​
    ​<script src="{{mix('js/admin.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    ​
    <script>
        $(document).ready(function() {
            $(".select2").select2();
            toastr.options = {
                "positionClass" : "toast-top-center",
                "closeButton" : true,
                "debug" : false,
                "newestOnTop" : true,
                "progressBar" : true,
                "preventDuplicates" : false,
                "onclick" : null,
                "showDuration" : "300",
                "hideDuration" : "1000",
                "timeOut" : "5000",
                "extendedTimeOut" : "1000",
                "showEasing" : "swing",
                "hideEasing" : "linear",
                "showMethod" : "fadeIn",
                "hideMethod" : "fadeOut"
            }
            @if(Session::has('success'))
                toastr['success']("{{ Session::get('success') }}")
            @endif
            @if(Session::has('warning'))
                toastr['warning']("{{ Session::get('warning') }}")
            @endif
            @if(Session::has('error'))
                toastr['error']("{{ Session::get('error') }}")
            @endif
        });
    </script>
    <script src="https://kit.fontawesome.com/7362fdfc7d.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    @yield('scripts')
</body>
​

</html>