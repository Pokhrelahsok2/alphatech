@extends('admin.layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox collapsed">
            <div class="ibox-title collapse-link">
                <h5>Add a New Team</h5>
                <div class="ibox-tools">
                    <a>
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <form action="{{ action('Admin\TeamController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" placeholder="" class="form-control
                                    @error('name') is-invalid @enderror">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="role">Role</label>
                                <input type="text" name="role" id="role" placeholder="" class="form-control
                                    @error('role') is-invalid @enderror">

                                @error('role')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea rows="1" id="description" class="form-control" name="description
                                    @error('description') is-invalid @enderror"></textarea>

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" name="image" id="image" placeholder="" class="form-control
                                    @error('image') is-invalid @enderror">

                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="facebook">Facebook </label>
                                <input type="text" name="facebook" id="facebook" placeholder="" class="form-control
                                    @error('facebook') is-invalid @enderror">

                                @error('facebook')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="instagram">Instagram </label>
                                <input type="text" name="instagram" id="instagram" placeholder="" class="form-control
                                    @error('instagram') is-invalid @enderror">

                                @error('instagram')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="twitter">Twitter </label>
                                <input type="text" name="twitter" id="twitter" placeholder="" class="form-control
                                    @error('twitter') is-invalid @enderror">

                                @error('twitter')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 text-left">
                            <div class="form-group">
                                <button class="btn btn-primary">Add Team</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>All Teams</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Description</th>
                            <th>Facebook</th>
                            <th>Instagram</th>
                            <th>Twitter</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($teams as $key=>$team)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $team->name }}</td>
                            <td>{{ $team->role }}</td>
                            <td>{{ $team->description }}</td>
                            <td>{{ $team->urls['facebook'] }}</td>
                            <td>{{ $team->urls['instagram'] }}</td>
                            <td>{{ $team->urls['twitter'] }}</td>
                            @if($team->image)
                            <td><a href="{{Storage::url($team->image)}}" target="_blank"> <img
                                        src="{{Storage::url($team->image)}}" width="80"></a></td>
                            @else
                            <td>No Image</td>
                            @endif
                            <th>
                                <form action="{{ action('Admin\TeamController@destroy', $team->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <a title="Edit This Team"
                                        href="{{ action('Admin\TeamController@edit', $team->id) }}"
                                        class="btn btn-info">
                                        <span class="fa fa-edit"></span> Edit
                                    </a>
                                    <button class="btn btn-danger"><span class="fa fa-trash"></span> Delete</button>
                                </form>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection