@extends('admin.layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Edit: {{$team->team}}</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
				<form action="{{action('Admin\TeamController@update',$team->id)}}" enctype="multipart/form-data"
					method="POST">
					@csrf
					@method('PUT')
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="name">Name</label>
								<input type="text" name="name" id="name" placeholder="" class="form-control @error('name') is-invalid @enderror"
									value="{{$team->name}}">

								@error('name')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="role">Role</label>
								<input type="text" name="role" id="role" placeholder="" class="form-control @error('role') is-invalid @enderror"
									value="{{$team->role}}">

								@error('role')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="description">Description</label>
								<textarea rows="1" id="description" class="form-control @error('description') is-invalid @enderror"
									name="description">{{$team->description}}</textarea>

								@error('description')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>


						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="facebook">Facebook </label>
								<input type="text" name="facebook" id="facebook" placeholder="" class="form-control @error('facebook') is-invalid @enderror"
									value="{{$team->urls['facebook']}}">

								@error('facebook')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="instagram">Instagram </label>
								<input type="text" name="instagram" id="instagram" placeholder="" class="form-control @error('instagram') is-invalid @enderror"
									value="{{$team->urls['instagram']}}">

								@error('instagram')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="twitter">Twitter </label>
								<input type="text" name="twitter" id="twitter" placeholder="" class="form-control @error('twitter') is-invalid @enderror"
									value="{{$team->urls['twitter']}}">

								@error('twitter')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="image">Image</label>
								<input type="file" name="image" id="image" placeholder="" class="form-control @error('image') is-invalid @enderror">

								@error('image')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<a href="{{Storage::url($team->image)}}" target="_blank"> <img
									src="{{Storage::url($team->image)}}" width="100"></a>
						</div>
						<div class="col-md-12 col-sm-12 text-right">
							<div class="form-group">
								<button class="btn btn-primary">Update Team</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection