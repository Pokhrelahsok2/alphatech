@extends('admin.layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox collapsed">
            <div class="ibox-title collapse-link">
                <h5>Add a New Service</h5>
                <div class="ibox-tools">
                    <a>
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <form action="{{ action('Admin\ServiceController@store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" placeholder="" class="form-control
                                    @error('title') is-invalid @enderror">

                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="skills">Skills</label>
                                <input type="text" name="skills" id="skills" placeholder="" class="form-control
                                    @error('skills') is-invalid @enderror">

                                @error('skills')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea rows="1" id="description" class="form-control" name="description
                                    @error('description') is-invalid @enderror"></textarea>

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 text-left">
                            <div class="form-group">
                                <button class="btn btn-primary">Add Services</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>All Services</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">

                <table class="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Title</th>
                            <th>Skills</th>
                            <th>Description</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($services as $key=>$service)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $service->title }}</td>
                            <td>{{ $service->skills }}</td>
                            <td>{{ $service->description }}</td>
                            <th>
                                <form action="{{ action('Admin\ServiceController@destroy', $service->id) }}"
                                    method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <a title="Edit This service"
                                        href="{{ action('Admin\ServiceController@edit', $service->id) }}"
                                        class="btn btn-info">
                                        <span class="fa fa-edit"></span> Edit
                                    </a>
                                    <button class="btn btn-danger"><span class="fa fa-trash"></span> Delete</button>
                                </form>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection