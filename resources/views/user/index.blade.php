@extends('user.layouts.app')
@section('content')
    <!-- Hero Section Begin -->
    <section class="hero-section" style="background-attachment:fixed;background-size:cover;background-position:center;background:url({{asset('storage/img/hero/hero-1.jpg')}});">
        <div class="hero__slider owl-carousel">
            <div class="hero__item" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <h1>IT Company in Nepal</h1>
                                <h5>Pioneer in IT since 2016</h5>
                                <a href="#" class="primary-btn">Work With Us</a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
                            <img src="{{asset('storage/img/hero/hero-right.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero__item" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="hero__text">
                                <h1>IT Company in Nepal</h1>
                                <h5>Pioneer in IT since 2016</h5>
                                <a href="#" class="primary-btn">Work With Us</a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="hero__img">
                            <img src="{{asset('storage/img/hero/hero-right.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->
    
    <!-- Register Domain Section Begin -->
    <section class="register-domain spad" style="position: relative">
        <div style="background:url({{asset('storage/img/clouds.png')}});width:100%; height: 120px;position: absolute;z-index:10;top:-120px">
        </div>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="register__text">
                        <div class="section-title">
                            <h3>About Us</h3>
                        </div>
                        <h5 style="text-align: center">Alphatech is an established and leading software company in the IT market providing comprehensive software development and web products since 2018. The core vision of Alphatech is to provide you the best IT solution.</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Register Domain Section End -->

    <!-- Services Section Begin -->
    <section class="services-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h3>OUR SERVICES</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <h5>Web Design</h5>
                        <span>HTML5 | CSS3 | BOOTSTRAP | JS</span>
                        <p>At Alphatech we focus at modern minimal designs, easy to use & pleasing to the eyes.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <h5>Web Development</h5>
                        <span>WORDPRESS | LARAVEL | NODE</span>
                        <p>We use a diverse set of languages and tools to develop a robust backend, both fast & reliable.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <h5>Web Hosting</h5>
                        <span>DIGITAL OCEAN</span>
                        <p>Do you want to host your website? No problem we have got your back there as well.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <h5>Logo & Banner Design</h5>
                        <span>ILLUSTRATOR | PHOTOSHOP</span>
                        <p>Our team of talented designers can create elegent & modern loooking logos & banners for your company.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <h5>Application Development</h5>
                        <span>VUE | FLUTTER</span>
                        <p>We develop robust SPAs or Native Applications for both IOS & Android platforms.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="services__item">
                        <h5>Online Advertising</h5>
                        <span>FACEBOOK</span>
                        <p>We can help you take your business to new heights using some online advertising sorcery.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services Section End -->


    <!-- Team Section Begin -->
    <section class="team-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="section-title normal-title">
                        <h3>Meet our team</h3>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="team__btn">
                        <a href="#" class="primary-btn">View all</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="team__item">
                        <div class="team__pic">
                        <img src="{{asset('storage/img/our-team/bipin.png')}}" alt="">
                        </div>
                        <div class="team__text">
                            <h5>Bipin Dhimal</h5>
                            <span>Chief executive officer</span>
                            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua.</p>
                            <div class="team__social">
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="team__item">
                        <div class="team__pic">
                            <img src="{{asset('storage/img/our-team/rishabh.png')}}" alt="">
                        </div>
                        <div class="team__text">
                            <h5>Rishabh Aryal</h5>
                            <span>Software engineer</span>
                            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua.</p>
                            <div class="team__social">
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="team__item">
                        <div class="team__pic">
                            <img src="{{asset('storage/img/our-team/ashok.png')}}" alt="">
                        </div>
                        <div class="team__text">
                            <h5>Ashok Pahadi</h5>
                            <span>Software engineer</span>
                            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua.</p>
                            <div class="team__social">
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="team__item">
                        <div class="team__pic">
                            <img src="{{asset('storage/img/our-team/royesh.png')}}" alt="">
                        </div>
                        <div class="team__text">
                            <h5>Royesh Thapa</h5>
                            <span>Product director</span>
                            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua.</p>
                            <div class="team__social">
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="team__item">
                        <div class="team__pic">
                            <img src="{{asset('storage/img/our-team/shubham.png')}}" alt="">
                        </div>
                        <div class="team__text">
                            <h5>Subham Ghimire</h5>
                            <span>Product director</span>
                            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua.</p>
                            <div class="team__social">
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="team__item">
                        <div class="team__pic">
                            <img src="{{asset('storage/img/our-team/rupesh.png')}}" alt="">
                        </div>
                        <div class="team__text">
                            <h5>Rupesh Dhakal</h5>
                            <span>Product director</span>
                            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua.</p>
                            <div class="team__social">
                                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Team Section End -->

    <!-- Testimonial Section Begin -->
    <section class="testimonial-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h3>Our Client say</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="testimonial__slider owl-carousel">
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                        <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                            <h5>Billie Eilish</h5>
                             <a><span>Designer </span></a>
                            <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                            <div class="testimonial__rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                            <h5>Billie Eilish</h5>
                            <span>Designer</span>
                            <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                            <div class="testimonial__rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                            <h5>Billie Eilish</h5>
                            <span>Designer</span>
                            <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                            <div class="testimonial__rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                            <h5>Billie Eilish</h5>
                            <span>Designer</span>
                            <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                            <div class="testimonial__rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                            <h5>Billie Eilish</h5>
                            <span>Designer</span>
                            <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                            <div class="testimonial__rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->


    <!-- Achievement Section Begin -->
<section class="achievement-section set-bg spad" data-setbg="{{asset('storage/img/achievement-bg.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="achievement__item">
                        <span class="fa fa-user-o"></span>
                        <h2 class="achieve-counter">20</h2>
                        <p>Clients</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="achievement__item">
                        <span class="fa fa-users"></span>
                        <h2 class="achieve-counter">10</h2>
                        <p>Employees</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="achievement__item">
                        <span class="fa fa-clone"></span>
                        <h2 class="achieve-counter">25</h2>
                        <p>Projects</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="achievement__item">
                        <span class="fa fa-coffee"></span>
                        <h2 class="achieve-counter">2468</h2>
                        <p>Cups of Coffee</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Achievement Section End -->
    
    
    <!-- Projects Section Start -->
    <section class="projects-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="section-title">
                        <h3>OUR PROJECTS</h3>
                    </div>
                </div>
            </div>
            <div class="row projects__list active projects__slider owl-carousel">
                <div class="col-12 ">
                    <div class="pricing__item">
                        <h4>CMS Development</h4>
                        <h3><span>Rojan Nirman Sewa</span></h3>
                        <p>Branch & Employee Management</p>
                        <a  class="primary-btn">View Project</a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pricing__item">
                        <h4>CMS Development</h4>
                        <h3><span>Rojan Nirman Sewa</span></h3>
                        <p>Branch & Employee Management</p>
                        <a  class="primary-btn">View Project</a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pricing__item">
                        <h4>CMS Development</h4>
                        <h3><span>Rojan Nirman Sewa</span></h3>
                        <p>Branch & Employee Management</p>
                        <a  class="primary-btn">View Project</a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pricing__item">
                        <h4>CMS Development</h4>
                        <h3><span>Rojan Nirman Sewa</span></h3>
                        <p>Branch & Employee Management</p>
                        <a  class="primary-btn">View Project</a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pricing__item">
                        <h4>CMS Development</h4>
                        <h3><span>Rojan Nirman Sewa</span></h3>
                        <p>Branch & Employee Management</p>
                        <a  class="primary-btn">View Project</a>
                    </div>
                </div>
                
            </div>
           
        </div>
    </section>
     <!-- Projects Section End -->

@endsection