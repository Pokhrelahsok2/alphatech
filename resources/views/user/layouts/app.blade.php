<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('storage/img/icon.png')}}" type="image/x-icon">
    <title>Alphatech Hetauda | Alphatech Nepal</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900&display=swap"
        rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
</head>

<body style="perspective: 2px;">
    @include('user.components.header')
    
    @yield('content')
    
    @include('user.components.footer')
    
    <script src="{{mix('js/app.js')}}"></script>
    
    @yield('scripts')
</body>

</html>