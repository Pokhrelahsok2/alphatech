@extends('user.layouts.app')
@section('content')
   <!-- Breadcrumb Begin -->
   <div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__option">
                    <a href="./index.html"><span class="fa fa-home"></span> Home</a>
                    <span>About</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- About Section Begin -->
<section class="about-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="about__img">
                    <img src="{{asset('storage/img/about-us.png')}}" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about__text">
                    <h2>Welcom to Alphatech</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo
                        viverra maecenas accumsan lacus vel facilisis.</p>
                    <div class="about__achievement">
                        <div class="about__achieve__item">
                            <span class="fa fa-user-o"></span>
                            <h4 class="achieve-counter">20</h4>
                            <p>Clients</p>
                        </div>
                        <div class="about__achieve__item">
                            <span class="fa fa-clone"></span>
                            <h4 class="achieve-counter">22</h4>
                            <p>Projects</p>
                        </div>
                        <div class="about__achieve__item">
                            <span class="fa fa-users"></span>
                            <h4 class="achieve-counter">12</h4>
                            <p>Employees</p>
                        </div>
                    </div>
                    <a href="#" class="primary-btn">Get started now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Section End -->

<!-- Services Section Begin -->
<section class="services-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>OUR SERVICES</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <h5>Web Design</h5>
                    <span>HTML5 | CSS3 | BOOTSTRAP | JS</span>
                    <p>At Alphatech we focus at modern minimal designs, easy to use & pleasing to the eyes.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <h5>Web Development</h5>
                    <span>WORDPRESS | LARAVEL | NODE</span>
                    <p>We use a diverse set of languages and tools to develop a robust backend, both fast & reliable.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <h5>Web Hosting</h5>
                    <span>DIGITAL OCEAN</span>
                    <p>Do you want to host your website? No problem we have got your back there as well.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <h5>Logo & Banner Design</h5>
                    <span>ILLUSTRATOR | PHOTOSHOP</span>
                    <p>Our team of talented designers can create elegent & modern loooking logos & banners for your company.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <h5>Application Development</h5>
                    <span>VUE | FLUTTER</span>
                    <p>We develop robust SPAs or Native Applications for both IOS & Android platforms.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="services__item">
                    <h5>Online Advertising</h5>
                    <span>FACEBOOK</span>
                    <p>We can help you take your business to new heights using some online advertising sorcery.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->


<!-- Team Section Begin -->
<section class="team-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <div class="section-title normal-title">
                    <h3>Meet our team</h3>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="team__btn">
                    <a href="#" class="primary-btn">View all</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="team__item">
                    <div class="team__pic">
                    <img src="{{asset('storage/img/our-team/bipin.png')}}" alt="">
                    </div>
                    <div class="team__text">
                        <h5>Bipin Dhimal</h5>
                        <span>Chief executive officer</span>
                        <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                            aliqua.</p>
                        <div class="team__social">
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="team__item">
                    <div class="team__pic">
                        <img src="{{asset('storage/img/our-team/rishabh.png')}}" alt="">
                    </div>
                    <div class="team__text">
                        <h5>Rishabh Aryal</h5>
                        <span>Software engineer</span>
                        <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                            aliqua.</p>
                        <div class="team__social">
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="team__item">
                    <div class="team__pic">
                        <img src="{{asset('storage/img/our-team/ashok.png')}}" alt="">
                    </div>
                    <div class="team__text">
                        <h5>Ashok Pahadi</h5>
                        <span>Software engineer</span>
                        <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                            aliqua.</p>
                        <div class="team__social">
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="team__item">
                    <div class="team__pic">
                        <img src="{{asset('storage/img/our-team/royesh.png')}}" alt="">
                    </div>
                    <div class="team__text">
                        <h5>Royesh Thapa</h5>
                        <span>Product director</span>
                        <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                            aliqua.</p>
                        <div class="team__social">
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="team__item">
                    <div class="team__pic">
                        <img src="{{asset('storage/img/our-team/shubham.png')}}" alt="">
                    </div>
                    <div class="team__text">
                        <h5>Subham Ghimire</h5>
                        <span>Product director</span>
                        <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                            aliqua.</p>
                        <div class="team__social">
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="team__item">
                    <div class="team__pic">
                        <img src="{{asset('storage/img/our-team/rupesh.png')}}" alt="">
                    </div>
                    <div class="team__text">
                        <h5>Rupesh Dhakal</h5>
                        <span>Product director</span>
                        <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                            aliqua.</p>
                        <div class="team__social">
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Team Section End -->

<!-- Testimonial Section Begin -->
<section class="testimonial-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Our Client say</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="testimonial__slider owl-carousel">
                <div class="col-lg-4">
                    <div class="testimonial__item">
                    <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                        <h5>Billie Eilish</h5>
                         <a><span>Designer </span></a>
                        <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                        <div class="testimonial__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial__item">
                        <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                        <h5>Billie Eilish</h5>
                        <span>Designer</span>
                        <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                        <div class="testimonial__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial__item">
                        <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                        <h5>Billie Eilish</h5>
                        <span>Designer</span>
                        <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                        <div class="testimonial__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial__item">
                        <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                        <h5>Billie Eilish</h5>
                        <span>Designer</span>
                        <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                        <div class="testimonial__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial__item">
                        <img src="{{asset('storage/img/testimonial/testimonial-1.jpg')}}" alt="">
                        <h5>Billie Eilish</h5>
                        <span>Designer</span>
                        <p>Very Professional & reliable group of young developers. These lads share a great lot of passion on every project. I am proud that I chose them to complete my Project.</p>
                        <div class="testimonial__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Testimonial Section End -->


<!-- Projects Section Start -->
<section class="projects-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="section-title">
                    <h3>OUR PROJECTS</h3>
                </div>
            </div>
        </div>
        <div class="row projects__list active projects__slider owl-carousel">
            <div class="col-12 ">
                <div class="pricing__item">
                    <h4>CMS Development</h4>
                    <h3><span>Rojan Nirman Sewa</span></h3>
                    <p>Branch & Employee Management</p>
                    <a  class="primary-btn">View Project</a>
                </div>
            </div>
            <div class="col-12">
                <div class="pricing__item">
                    <h4>CMS Development</h4>
                    <h3><span>Rojan Nirman Sewa</span></h3>
                    <p>Branch & Employee Management</p>
                    <a  class="primary-btn">View Project</a>
                </div>
            </div>
            <div class="col-12">
                <div class="pricing__item">
                    <h4>CMS Development</h4>
                    <h3><span>Rojan Nirman Sewa</span></h3>
                    <p>Branch & Employee Management</p>
                    <a  class="primary-btn">View Project</a>
                </div>
            </div>
            <div class="col-12">
                <div class="pricing__item">
                    <h4>CMS Development</h4>
                    <h3><span>Rojan Nirman Sewa</span></h3>
                    <p>Branch & Employee Management</p>
                    <a  class="primary-btn">View Project</a>
                </div>
            </div>
            <div class="col-12">
                <div class="pricing__item">
                    <h4>CMS Development</h4>
                    <h3><span>Rojan Nirman Sewa</span></h3>
                    <p>Branch & Employee Management</p>
                    <a  class="primary-btn">View Project</a>
                </div>
            </div>
            
        </div>
       
    </div>
</section>
 <!-- Projects Section End -->

@endsection