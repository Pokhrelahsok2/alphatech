<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-3">
            <div class="header__logo">
            <a href="./index.html"><img src="{{asset('storage/img/logo.png')}}" alt=""></a>
            </div>
        </div>
        <div class="col-lg-9 col-md-9">
            <nav class="header__menu">
                <ul>
                    <li {{ (Request::is('/') ? 'class=active' : '') }}><a href="{{action('HomeController@showIndexPage')}}">Home</a></li>
                    <li {{ (Request::is('about-us') ? 'class=active' : '') }}><a href="{{action('HomeController@showAboutPage')}}">About</a></li>
                    <li {{ (Request::is('blogs') ? 'class=active' : '') }}><a href="{{action('BlogController@index')}}">Blog</a></li>
                    <li {{ (Request::is('contact-us') ? 'class=active' : '') }}><a href="{{action('HomeController@showContactPage')}}">Contact</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="canvas__open">
        <span class="fa fa-bars"></span>
    </div>
</div>