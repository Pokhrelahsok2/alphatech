<footer class="footer-section">
    <div class="footer__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="footer__top-call">
                        <h5>Have a project ? Contact Us.</h5>
                        <h2>+977 9824289806</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="footer__text set-bg" style="background-position: top;background-size:cover" data-setbg="{{asset('storage/img/footer-bg.png')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="footer__text-about">
                        <div class="footer__logo">
                        <a href="./index.html"><img src="{{asset('storage/img/logo.png')}}" alt=""></a>
                        </div>
                        <p>Alphatech is a young software company in Hetauda, Nepal. We are a young group of talented developers & designers who are destined to change the face of IT in Nepal.</p>
                        <div class="footer__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-youtube-play"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="footer__text-widget">
                        <h5>Company</h5>
                        <ul>
                            <li><a href="#">Company</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Blogs</a></li>
                            <li><a href="#">Careers</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="footer__text-widget">
                        <h5>Services</h5>
                        <ul>
                            <li><a href="#">Web Design</a></li>
                            <li><a href="#">Web Development</a></li>
                            <li><a href="#">Mobile Development</a></li>
                            <li><a href="#">Web Hosting</a></li>
                            <li><a href="#">Online Adverts</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="footer__text-widget">
                        <h5>CONTACT US</h5>
                        <ul class="footer__widget-info">
                            <li><span class="fa fa-map-marker"></span> School Road, Hetauda, Nepal</li>
                            <li><span class="fa fa-mobile"></span> +977 9824289806 | +977 9845027087</li>
                            <li><span class="fa fa-headphones"></span> info@alphatech.com.np</li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</footer>