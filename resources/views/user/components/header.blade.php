 <!-- Page Preloder -->
 <div id="preloder">
    <div class="loader"></div>
</div>

<!-- Offcanvas Menu Begin -->
<div class="offcanvas__menu__overlay"></div>

<div class="offcanvas__menu__wrapper">
    <div class="canvas__close">
        <span class="fa fa-times-circle-o"></span>
    </div>
    <div class="offcanvas__logo">
    <a href="#"><img src="{{asset('storage/img/logo.png')}}" alt=""></a>
    </div>
    <nav class="offcanvas__menu mobile-menu">
        <ul>
            <li {{ (Request::is('/') ? 'class=active' : '') }}><a href="{{action('HomeController@showIndexPage')}}">Home</a></li>
            <li {{ (Request::is('about-us') ? 'class=active' : '') }}><a href="{{action('HomeController@showAboutPage')}}">About</a></li>
            <li {{ (Request::is('blogs') ? 'class=active' : '') }}><a href="{{action('BlogController@index')}}">Blog</a></li>
            <li {{ (Request::is('contact-us') ? 'class=active' : '') }}><a href="{{action('HomeController@showContactPage')}}">Contact</a></li>
        </ul>
    </nav>
    <div id="mobile-menu-wrap"></div>
    <div class="offcanvas__info">
        <ul>
            <li><span class="icon_phone"></span>+977 9824289806</li>
            <li><span class="fa fa-envelope"></span>info@alphatech.com.np</li>
            <li><span class="fa fa-map-marker"></span>Hetauda, Nepal</li>
        </ul>
    </div>
</div>
<!-- Offcanvas Menu End -->

<!-- Header Section Begin -->
<header class="header-section header-normal">
    <div class="header__info">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="header__info-left">
                        <ul>
                            <li><span class="icon_phone"></span>+977 9824289806</li>
                            <li><span class="fa fa-envelope"></span>info@alphatech.com.np</li>
                            <li><span class="fa fa-map-marker"></span>School Road, Hetauda, Nepal</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('user/components/navbar')
</header>
<!-- Header End -->