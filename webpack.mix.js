const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
    'resources/js/jquery-3.3.1.min.js',
    'resources/js/jquery.slicknav.js',
    'resources/js/bootstrap.min.js',
    'resources/js/owl.carousel.min.js',
    'resources/js/main.js',
], 'public/js/app.js', 'public/js');


mix.combine([
    'resources/admin/js/jquery-3.1.1.min.js',
    'resources/admin/js/popper.min.js',
    'resources/admin/js/bootstrap.js',
    'resources/admin/js/plugins/metisMenu/jquery.metisMenu.js',
    'resources/admin/js/plugins/slimscroll/jquery.slimscroll.min.js',
    'resources/admin/js/inspinia.js',
    'resources/admin/js/plugins/pace/pace.min.js',
    'resources/admin/js/plugins/toastr/toastr.min.js',
], 'public/js/admin.js', 'public/js');




mix.styles([
    'resources/admin/css/bootstrap.min.css',
    'resources/admin/css/animate.css',
    'resources/admin/css/style.css',
    'resources/admin/css/plugins/toastr/toastr.min.css',
], 'public/css/admin.css');


mix.styles([
    'resources/css/bootstrap.min.css',
    'resources/css/elegant-icons.css',
    'resources/css/flaticon.css',
    'resources/css/font-awesome.min.css',
    'resources/css/owl.carousel.min.css',
    'resources/css/slicknav.min.css',
    'resources/css/style.css'
], 'public/css/app.css');
