<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function showIndexPage()
    {
        return view('user.index');
    }
    public function showAboutPage()
    {
        return view('user.about');
    }
    public function showContactPage()
    {
        return view('user.contact');
    }
    public function showAdminDashboard()
    {
        return view('admin.dashboard');
    }
}
