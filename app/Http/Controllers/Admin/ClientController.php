<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required|unique:clients',
            'image' => 'required|image',
            'description' => 'required'
        ]);


        $imgpath = $request->file('image')->store('client', ['disk' => 'public']);
        $client = new client();
        $client->name = $request->get('name');
        $client->role = $request->get('role');
        $client->description = $request->get('description');
        $client->image = $imgpath;


        $client->save();

        return redirect()->back()->with('success', 'Clients member added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $clients = Client::all();
        if (!$client) {
            return redirect()->back()->with('warning', 'The  client member you wanted to edit does not exist.');
        }

        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        if (!$client) {
            return redirect()->back()->with('warning', 'The client member you wanted to edit does not exist.');
        }
        $this->validate($request, [
            'name' => 'required|unique:clients,name,' . $client->id,
            'description' => 'required',


        ]);


        if ($request->hasFile('image')) {
            $imgpath = $request->file('image')->store('client', ['disk' => 'public']);
            $client->image = $imgpath;
        }
        $client->name = $request->get('name');
        $client->role = $request->get('role');
        $client->description = $request->get('description');


        $client->save();

        return redirect()->action('Admin\ClientController@index')->with('success', 'Client member updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        if ($client) {
            $client->delete();
        }

        return redirect()->action('Admin\ClientController@index')->with('success', 'Client member deleted successfully!');
    }
}
