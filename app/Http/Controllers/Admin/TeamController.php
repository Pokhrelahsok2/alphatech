<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        return view('admin.teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required|unique:teams',
            'image' => 'required|image',
            'description' => 'required'
        ]);
        $urls = [];
        $urls['facebook'] = $request['facebook'] ?? "";
        $urls['instagram'] = $request['instagram'] ?? "";
        $urls['twitter'] = $request['twitter'] ?? "";

        $imgpath = $request->file('image')->store('team', ['disk' => 'public']);
        $team = new Team();
        $team->name = $request->get('name');
        $team->role = $request->get('role');
        $team->description = $request->get('description');
        $team->image = $imgpath;
        $team->urls = $urls;

        $team->save();

        return redirect()->back()->with('success', 'Team member added successfully!');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $teams = Team::all();
        if (!$team) {
            return redirect()->back()->with('warning', 'The  team member you wanted to edit does not exist.');
        }

        return view('admin.teams.edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        if (!$team) {
            return redirect()->back()->with('warning', 'The team member you wanted to edit does not exist.');
        }
        $this->validate($request, [
            'name' => 'required|unique:teams,name,' . $team->id,
            'description' => 'required',


        ]);
        $urls = [];
        $urls['facebook'] = $request['facebook'] ?? "";
        $urls['instagram'] = $request['instagram'] ?? "";
        $urls['twitter'] = $request['twitter'] ?? "";

        if ($request->hasFile('image')) {
            $imgpath = $request->file('image')->store('team', ['disk' => 'public']);
            $team->image = $imgpath;
        }
        $team->name = $request->get('name');
        $team->role = $request->get('role');
        $team->description = $request->get('description');
        $team->urls = $urls;

        $team->save();

        return redirect()->action('Admin\TeamController@index')->with('success', 'Team member updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        if ($team) {
            $team->delete();
        }

        return redirect()->action('Admin\TeamController@index')->with('success', 'Team member deleted successfully!');
    }
}
