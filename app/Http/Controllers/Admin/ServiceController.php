<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title' => 'required|unique:services',
            'description' => 'required',
            'skills' => 'required'
        ]);

        $service = new Service();
        $service->title = $request->get('title');
        $service->skills = $request->get('skills');
        $service->description = $request->get('description');
        $service->save();

        return redirect()->back()->with('success', 'Services  added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $services = Service::all();
        if (!$service) {
            return redirect()->back()->with('warning', 'The  services you wanted to edit does not exist.');
        }

        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {

        if (!$service) {
            return redirect()->back()->with('warning', 'The service  you wanted to edit does not exist.');
        }
        $this->validate($request, [
            'title' => 'required|unique:services,title,' . $service->id,
            'description' => 'required',
            'skills' => 'required'
        ]);

        $service->title = $request->get('title');
        $service->skills = $request->get('skills');
        $service->description = $request->get('description');

        $service->save();

        return redirect()->action('Admin\ServiceController@index')->with('success', 'Service  updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if ($service) {
            $service->delete();
        }

        return redirect()->action('Admin\ServiceController@index')->with('success', 'Services  deleted successfully!');
    }
}
