<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'type' => 'required|unique:projects',
            'title' => 'required',
            'description' => 'required'
        ]);

        $project = new Project();
        $project->type = $request->get('type');
        $project->title = $request->get('title');
        $project->links = $request->get('links');
        $project->description = $request->get('description');
        $project->save();

        return redirect()->back()->with('success', 'Projects  added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $projects = Project::all();
        if (!$project) {
            return redirect()->back()->with('warning', 'The  projects you wanted to edit does not exist.');
        }

        return view('admin.projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        if (!$project) {
            return redirect()->back()->with('warning', 'The project  you wanted to edit does not exist.');
        }
        $this->validate($request, [
            'title' => 'required|unique:projects,title,' . $project->id,
            'description' => 'required',
            'type' => 'required'
        ]);

        $project->type = $request->get('type');
        $project->title = $request->get('title');
        $project->links = $request->get('links');
        $project->description = $request->get('description');
        $project->save();

        return redirect()->action('Admin\ProjectController@index')->with('success', 'Project  updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        if ($project) {
            $project->delete();
        }

        return redirect()->action('Admin\ProjectController@index')->with('success', 'Project  deleted successfully!');
    }
}
